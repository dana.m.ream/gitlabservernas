#!/bin/sh

# Generate self signed certs and put them where they are needed.
mkdir -p /etc/gitlab/trusted-certs
openssl version
openssl req -newkey rsa:4096 -nodes -sha256 -keyout /etc/gitlab/trusted-certs/$LAN_IP.key -x509 -days 11499 -out /etc/gitlab/trusted-certs/$LAN_IP.crt -subj "/C=US/ST=CA/L=town/O=org/OU=0/CN=localhost" -addext "subjectAltName=DNS:localhost,IP:$LAN_IP"
mkdir -p /etc/gitlab/ssl
cp /etc/gitlab/trusted-certs/$LAN_IP.crt /etc/gitlab/ssl/$LAN_IP.crt
cp /etc/gitlab/trusted-certs/$LAN_IP.key /etc/gitlab/ssl/$LAN_IP.key

# Start GitLab server.
/assets/wrapper

