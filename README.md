A GitLab server docker image that stores repos on a NAS.

Gitlab docker using NFS volumes works well. Make sure the NFS share in /etc/exports has no_root_squash specified, or else Gitlab will not be able to use chmod or chown, which it needs.

To configure a runner to connect to the GitLab server at IP XXX.XXX.XXX.XXX:

docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register --non-interactive --executor "docker" --docker-image alpine-v12.6.0 --url "http://XXX.XXX.XXX.XXX/" --registration-token "4VKbXProRi1qwqBs-mx8" --description "docker-runner" --tag-list "docker,amd64" --run-untagged="true" --locked="false" --access-level="not_protected"


To configure a runner for use with docker-compose:

docker run --rm --network="gitlab-net" -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register --non-interactive --executor "docker" --docker-image alpine-v12.6.0 --url "http://gitlab-lan/" --registration-token "4VKbXProRi1qwqBs-mx8" --description "docker-runner" --tag-list "docker,amd64" --run-untagged="true" --locked="false" --access-level="not_protected"


To just run the runner after configuration:
sudo docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner
